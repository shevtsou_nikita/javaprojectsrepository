package api.repository;

import impl.model.Customer;

import java.util.List;

public interface ICustomerRepository {
    int insert(Customer newCustomer);

    List<Customer> getAll();

    Customer findById(int id);

    List<Customer> findByName(String name);

    void update(Customer newCustomer);

    void deleteById(int id);
}
