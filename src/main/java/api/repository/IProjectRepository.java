package api.repository;

import impl.model.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

public interface IProjectRepository {
    public int insert(Project newProject);

    public List<Project> getAll();

    public List<Project> findByCustomerId(int id);

    public Project findById(int id);

    public void update(Project newProject);

    public void update(List<Project> newProjects);

    public void deleteById(int id);

    public void deleteByCustomerId(int customerId);

}
