package api.service;

import impl.model.Customer;
import impl.model.Project;

import java.util.List;

public interface ICustomerService {

    Customer getById(Integer id);

    List<Customer> getAll();

    List<Customer> getByName(String name);

    void add(Customer customer);

    void addProject(Customer customer, Project newProject);

    void update(Customer newCustomer);

    void deleteProject(Customer customer, Project targetProject);

    void delete(Customer customer);

    void deleteById(Integer id);
}

