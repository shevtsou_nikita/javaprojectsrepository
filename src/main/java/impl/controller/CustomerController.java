package impl.controller;

import api.service.ICustomerService;
import dto.CustomerDto;
import impl.model.Customer;
import impl.validator.CustomerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerValidator validator;
    @Autowired
    private ICustomerService customerService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getCustomers(Model model) {

        model.addAttribute("customers", customerService.getAll());
        return "customers";
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public String editCustomer(@RequestParam(value = "id", required = false) Integer id, Model model) {
        if (id == null) {
            Customer customer = new Customer();
            model.addAttribute("customer", new CustomerDto(customer));
            model.addAttribute("projects", null);
            return "customer";
        } else {
            Customer originalCustomer = customerService.getById(id);
            model.addAttribute("customer", new CustomerDto(originalCustomer));
            model.addAttribute("projects", originalCustomer.getProjectList());
            return "customer";
        }
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public String saveCustomer( @ModelAttribute("customer") CustomerDto customer, BindingResult result, Model model) {
        validator.validate(customer, result);
        if (result.hasErrors()) {
            model.addAttribute("projects",(customerService.getById(customer.getId()).getProjectList()));
            return "customer";
        }
        Customer originalCustomer = customerService.getById(customer.getId());
        if (originalCustomer != null) {
            originalCustomer.setName(customer.getName());
            originalCustomer.setDescription(customer.getDescription());
            customerService.update(originalCustomer);
            return "redirect:/customer/";
        } else {
            originalCustomer = new Customer(customer.getName(), customer.getDescription(), null);
            customerService.add(originalCustomer);
            return "redirect:/customer/";
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteCustomer(@RequestParam(value = "id", required = true) Integer id, Model model) {
        customerService.deleteById(id);
        return "redirect:/customer/";
    }
}
