package impl.controller;

import api.service.ICustomerService;
import impl.model.Customer;
import impl.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Controller
@RequestMapping("/project")
public class ProjectController {
    @Autowired
    private ICustomerService customerService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String editProject(@RequestParam(value = "id", required = false) Integer id,
                              @RequestParam(value = "customerId") Integer customerId,
                              Model model) {
        if (id == null) {
            Customer customer = customerService.getById(customerId);
            Project project = new Project("-", new Date(), "-", customer);
            customerService.addProject(customer, project);
            model.addAttribute("project", project);
            return "project";
        } else {
            Project targetProject = new Project();
            for (Project project : customerService.getById(customerId).getProjectList()) {
                if (project.getId() == id) {
                    targetProject = project;
                    break;
                }
            }
            model.addAttribute("project", targetProject);
            return "project";
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String editProject(@RequestParam(value = "customerId") Integer customerId,
                              @ModelAttribute(value = "project") Project project,
                              Model model) {
        customerService.update(customerService.getById(customerId));
        return "redirect:/customer/customer?id=" + customerId;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteProject(@RequestParam(value = "id") Integer id,
                                @RequestParam(value = "customerId") Integer customerId) {
        Customer customer = customerService.getById(customerId);
        for (Project project : customer.getProjectList()) {
            if (project.getId() == id) {
                customer.deleteProject(project);
                break;
            }
        }
        customerService.update(customer);
        return "redirect:/customer/customer?id=" + customerId;
    }
}
