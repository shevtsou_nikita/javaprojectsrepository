package impl.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Entity
@Table(name = "Customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "c_increment")
    @SequenceGenerator(name = "c_increment", sequenceName = "c_increment", allocationSize = 1)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description")
    private String description;
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL,
            orphanRemoval = true, targetEntity = Project.class, fetch = FetchType.EAGER)
    private List<Project> projectList = new ArrayList<>();

    public Customer(String name, String description, List<Project> projectList) {
        this.name = name;
        this.description = description;
        if (projectList != null)
            this.projectList.addAll(projectList);

    }

    public Customer() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Project> getProjectList() {
        projectList.sort(new Comparator<Project>() {
            @Override
            public int compare(Project p1, Project p2) {
                return p1.getId() - p2.getId();
            }
        });
        return this.projectList;
    }

    public void setProjectList(List<Project> projectList) {
        if (projectList != null) {
            this.projectList.clear();
            this.projectList.addAll(projectList);
        }
    }

    public void addProject(Project newProject) {
        if (newProject == null) return;
        projectList.add(newProject);
    }

    public void deleteProject(Project targetProject) {
        projectList.remove(targetProject);
    }

    public String toString() {
        return new String("Id = " + id + ",Name = " + name.trim() + ",Description = " + description.trim() + ",Projects = " + projectList.toString());
    }
}
