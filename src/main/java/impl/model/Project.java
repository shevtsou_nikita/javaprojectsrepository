package impl.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "p_increment")
    @SequenceGenerator(name = "p_increment", sequenceName = "p_increment", allocationSize = 1)
    @Column(name = "id")
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "date", nullable = false)
    @Temporal(value = TemporalType.DATE)
    private Date date;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "customerid", nullable = false)
    private Customer customer;

    public Project(String name, Date start, String description, Customer customer) {

        this.name = name;
        this.date = start;
        this.description = description;
        this.customer = customer;
    }

    public Project() {
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String toString() {
        return new String("Id = " + id + ",Name = " + name.trim() + ",Start Date = "
                + date + ",Description = " + description.trim() + ",CustomerId = " + customer.getId());
    }
}
