package impl.repository;

import api.repository.ICustomerRepository;
import impl.model.Customer;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepository implements ICustomerRepository {
    @Autowired
    private SessionFactory sessionFactory;


    public int insert(Customer newCustomer) {
        Session session = sessionFactory.getCurrentSession();
        Integer id = (Integer) session.save(newCustomer);
        return id;

    }

    public List<Customer> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List customers = session.createQuery("FROM Customer order by  id").list();
        return customers;

    }

    public Customer findById(int id) {
        Session session = sessionFactory.openSession();
        Customer customer = (Customer) session.get(Customer.class, id);
        return customer;

    }

    public List<Customer> findByName(String name){
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Customer.class)
                .add(Restrictions.ilike("name",name))
                .list();

    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        List<Customer> test = null;
        test = getAll();
        for (int x = 0; x < test.size(); x++) {
            Customer c = test.get(x);
            result.append(String.format("| %d | %s | %s | \n", c.getId(), c.getName(), c.getDescription().trim()));
        }
        result.append("--------------------------");
        return result.toString();
    }

    public void update(Customer newCustomer) {
        Session session = sessionFactory.getCurrentSession();
        Customer targetCustomer = (Customer) session.get(Customer.class, newCustomer.getId());
        targetCustomer.setName(newCustomer.getName());
        targetCustomer.setDescription(newCustomer.getDescription());
        targetCustomer.setProjectList(newCustomer.getProjectList());
        session.merge(targetCustomer);
    }

    public void deleteById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Customer targetCustomer = (Customer) session.get(Customer.class, id);
        session.delete(targetCustomer);

    }

}
