package impl.repository;

import impl.model.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ProjectRepository {
    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;


    public int insert(Project newProject) {
        Session session = sessionFactory.getCurrentSession();
        int id = (Integer) session.save(newProject);
        return id;
    }

    public List<Project> getAll() {
        Session session = sessionFactory.getCurrentSession();
        List projects = session.createQuery("FROM Project order by id").list();
        return projects;

    }

    public List<Project> findByCustomerId(int id) {
        Session session = sessionFactory.getCurrentSession();
        List projects = session.createQuery("FROM Project WHERE CustomerId=" + id).list();
        return projects;

    }

    public Project findById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Project project = (Project) session.get(Project.class, id);
        return project;
    }

    public void update(Project newProject) {
        Session session = sessionFactory.getCurrentSession();
        Project targetProject = (Project) session.get(Project.class, newProject.getId());
        targetProject.setName(newProject.getName());
        targetProject.setDescription(newProject.getDescription());
        targetProject.setDate(newProject.getDate());
        targetProject.setCustomer(newProject.getCustomer());
        session.saveOrUpdate(targetProject);
    }

    public void update(List<Project> newProjects) {
        for (Project newProject : newProjects) {
            int newProjectId = newProject.getId();
            boolean duplicate = (findById(newProjectId) != null);
            if (duplicate)
                update(newProject);
            else insert(newProject);
        }
    }

    public void deleteById(int id) {
        Session session = sessionFactory.getCurrentSession();
        Project targetProject = (Project) session.get(Project.class, id);
        session.delete(targetProject);

    }

    public void deleteByCustomerId(int customerId) {
        Session session = sessionFactory.getCurrentSession();
        List<Project> projects = session.createQuery("FROM Project WHERE CustomerId=" + customerId).list();
        for (Project project : projects)
            session.delete(project);
    }

}
