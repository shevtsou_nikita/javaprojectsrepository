package impl.service;

import api.repository.ICustomerRepository;
import api.repository.IProjectRepository;
import api.service.ICustomerService;
import impl.model.Customer;
import impl.model.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("customerService")
@Transactional(readOnly = true)
public class CustomerService implements ICustomerService {
    @Autowired
    private ICustomerRepository customerRepository;

    public Customer getById(Integer id) {
        return customerRepository.findById(id);
    }

    public List<Customer> getByName(String name){
       return customerRepository.findByName(name);
    }

    public List<Customer> getAll() {
        return customerRepository.getAll();
    }

    @Transactional(readOnly = false)
    public void add(Customer customer) {
        customerRepository.insert(customer);
    }

    @Transactional(readOnly = false)
    public void addProject(Customer customer, Project newProject) {
        customer.addProject(newProject);
        customerRepository.update(customer);
    }

    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        List<Customer> customers = getAll();
        if (customers.size() == 0) {
            return stringBuilder.append("NOTHING\n").toString();
        }
        for (Customer customer : customers) {
            stringBuilder.append(customer.toString() + "\n");
        }
        return stringBuilder.toString();
    }

    @Transactional(readOnly = false)
    public void update(Customer newCustomer) {
        customerRepository.update(newCustomer);
    }

    @Transactional(readOnly = false)
    public void deleteProject(Customer customer, Project targetProject) {
        customer.deleteProject(targetProject);
        customerRepository.update(customer);
    }

    @Transactional(readOnly = false)
    public void delete(Customer customer) {
        customerRepository.deleteById(customer.getId());
    }

    @Transactional(readOnly = false)
    public void deleteById(Integer id) {
        customerRepository.deleteById(id);
    }
}
