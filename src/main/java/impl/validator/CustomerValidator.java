package impl.validator;

import api.service.ICustomerService;
import dto.CustomerDto;
import impl.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

@Component
public class CustomerValidator implements Validator {
    @Autowired
    private ICustomerService customerService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(CustomerDto.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        List<Customer> customers = customerService.getAll();
        CustomerDto customerDto = (CustomerDto) target;
        String name = (customerDto.getName()).toLowerCase();
        Integer id = customerDto.getId();

        if (name.trim().equals("")) {
            errors.rejectValue("name", "error.emptyName");
            return;
        }

        if (!name.matches("[a-zа-я]+")) {
            errors.rejectValue("name", "error.invalidName");
            return;
        }
        boolean duplicate = false;
        for (Customer customer : customerService.getByName(name)) {
            if (customerDto.getId() != (customer.getId())) {
                errors.rejectValue("name", "error.duplicateName");
                break;
            }
        }


    }
}
