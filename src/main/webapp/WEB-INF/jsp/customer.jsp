<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><spring:message code="label.customer.titleName"/></title>
</head>
<body>
<h2>Edit customer</h2>
<form:form method="POST" action="/customer/customer?id=${customer.id}" modelAttribute="customer">
    <table>
        <tr>
            <td><form:label path="id"><spring:message code="label.customer.id"/></form:label></td>
            <td><form:input path="id" disabled="true"/></td>
        </tr>
        <tr>
            <td><form:label path="name"><spring:message code="label.customer.name"/></form:label></td>
            <td><form:input path="name"/></td>
            <td><form:errors path="name" cssClass="error"/></td>
        </tr>
        <tr>
            <td><form:label path="description"><spring:message code="label.customer.description"/></form:label></td>
            <td><form:input path="description"/></td>
        </tr>
    </table>
    <h2><spring:message code="label.project.title"/></h2>
    <table border="1">
        <c:forEach var="project" items="${projects}">
            <tr>
                <td><c:out value="${project.id}"/></td>
                <td><c:out value="${project.name}"/></td>
                <td><c:out value="${project.date}"/></td>
                <td><c:out value="${project.description}"/></td>
                <td><a href="/project/?id=${project.id}&customerId=${customer.id}"><spring:message code="label.edit"/></a>
                <td>
                <td><a href="/project/delete?id=${project.id}&customerId=${customer.id}"><spring:message code="label.delete"/></a></td>
            </tr>
        </c:forEach>
    </table>
    <a href="/project/?customerId=${customer.id}"><spring:message code="label.add"/></a>
    <input type="submit" value=<spring:message code="label.button.submit"/>>
    <input type="reset" value=<spring:message code="label.button.reset"/>>
</form:form>
</body>
</html>
