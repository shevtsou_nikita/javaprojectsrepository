<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customers</title>
</head>
<body>
<table border="1">

    <c:forEach var="Customer" items="${customers}">
        <tr>
            <td><c:out value="${Customer.id}"/></td>
            <td><c:out value="${Customer.name}"/></td>
            <td><c:out value="${Customer.description}"/></td>
            <td><a href="/customer/customer?id=${Customer.id}"><spring:message code="label.edit"/></a></td>
            <td><a href="/customer/delete?id=${Customer.id}"><spring:message code="label.delete"/></a></td>
        </tr>
    </c:forEach>
</table>
<a href="/customer/customer"><spring:message code="label.add"/></a>
</body>
</html>
