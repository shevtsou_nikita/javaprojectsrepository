<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><spring:message code="label.project.editPageTitle"/></title>
</head>
<body>
<%request.setCharacterEncoding("UTF-8");%>
<form:form method="POST" action="/project/?customerId=${project.customer.id}" modelAttribute="project">
    <table>
        <tr>
            <td><form:label path="id"><spring:message code="label.project.id"/></form:label></td>
            <td><form:input path="id" disabled="true"/>
        </tr>
        <tr>
            <td><form:label path="name"><spring:message code="label.project.name"/></form:label></td>
            <td><form:input path="name"/>
        </tr>
        <tr>
            <td><form:label path="date" value="yyyy/MM/dd"><spring:message code="label.project.date"/></form:label></td>
            <td><form:input path="date"/>
        </tr>
        <tr>
            <td><form:label path="description"><spring:message code="label.project.description"/></form:label></td>
            <td><form:input path="description"/>
        </tr>
    </table>
    <input type="submit" value=<spring:message code="label.button.submit"/>>
    <input type="reset" value=<spring:message code="label.button.reset"/>>
</form:form>
</body>
</html>
